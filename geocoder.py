from bottle import route, run, template
import string
import urllib.request
import urllib.parse
import json
location = []
x = 0
@route('/geoCodeRequest/2/<name>')
def geoCodeRequest(name):
    x = 0
    #print(name)
    #print(name[:4])
    #print(name[:3])
    with open('ukPostcode.csv','r') as f:
        while True:
            line = f.readline()
            (line)
            if not line: break
            locationDetails = line.split(',')
            (locationDetails[0])
            if locationDetails[0] == ('"' + name[:4].replace("%","").replace("+","").replace("\xa0","") + '"'):
                coords = locationDetails[3] + ", " + locationDetails[4]
                return coords
            elif locationDetails[0] == name[:4].replace("%","").replace("+","").replace("\xa0",""):
                coords = locationDetails[3] + ", " + locationDetails[4]
                return ((coords.replace('"','')))
            elif locationDetails[0] == ('"' + name[:3].replace("%","").replace("+","").replace("\xa0","") + '"'):
                coords = locationDetails[3] + ", " + locationDetails[4]
                return coords
            elif locationDetails[0] == name[:3].replace("%","").replace("+","").replace("\xa0",""):
                coords = locationDetails[3] + ", " + locationDetails[4]
                return ((coords.replace('"','')))
            elif name[:2] == "M5":
                coords = "53.47" + ", " + "-2.28"
                return ((coords.replace('"','')))
            else:
                ("no match found for " + name)
                x = x + 1
    f.close()
    f.close()
@route('/geoCodeRequest/1/<name>')
def geoCodeRequest(name):
    parsed_json = ""
    response = ""
    response = urllib.request.urlopen('https://maps.googleapis.com/maps/api/geocode/json?address=' + name + '&key=AIzaSyCbjPvJNz8Ez6iek5qn8zXU0MVUNqXRA1I').read().decode('utf-8')
    parsed_json = json.loads(response)
    try:
        return (str(parsed_json['results'][0]['geometry']['location']['lat']) + ',' + str(parsed_json['results'][0]['geometry']['location']['lng']))
    except BaseException as e:
        return ("No match found for" + name)
run(host='localhost', port=80)
