# Import the modules required for the solution
import folium
from bottle import route, run, template, static_file, request, ServerAdapter
import urllib.request
import urllib.parse
import os
import heatMap
import re
import platform
import threading
#import Crypto
#from Crypto.Cipher import AES
import datetime
# Initialise orders array
orders = []
orderDates = []
mapOrders = []
coords = []
filters =[]
addresses = []
heatOrders = []
# Give the user the option to use SSL encryption
sslInput = "Y"
if(sslInput == "Y"):
    sslPath = input("Please give the filename of your SSL certificate: ")
    class SSLWSGIRefServer(ServerAdapter):
        def run(self, handler):
            from wsgiref.simple_server import make_server, WSGIRequestHandler
            import ssl
            if self.quiet:
                class QuietHandler(WSGIRequestHandler):
                    def log_request(*args, **kw): pass
                self.options['handler_class'] = QuietHandler
            srv = make_server(self.host, self.port, handler, **self.options)
            srv.socket = ssl.wrap_socket (
                                      srv.socket,
                                      certfile=sslPath,  # path to certificate
                                      server_side=True)
            srv.serve_forever()
# Set the host ip and the port that the server will listen on
hostIp = input("Please enter the host ip:")
port = 8080
""" Execute a shell script which will start the geocoding module)
"""
if(platform.system() == "Darwin" or platform.system() == "Linux"):
    os.system('sh geocoder.sh &')
elif(platform.system() == "Windows"):
    os.system('start cmd /k python geocoder.py')
else:
    print("OS could not be determined, attempting to run shell script")
    os.system('sh geocoder.sh &')
# Request module:
# If the client browser requests a static file (i.e an image embedded in another page) the following code will be executed:
@route("/static/<sfile>")
def send_static(sfile):
    return static_file(sfile,root='')
@route("/")
def index():
    return '<meta http-equiv="refresh" content="0; url=/login">'
# If the client browser requests the map page this code will be executed:
def mappingFunction():
# Calls the sort function to sort through all the data according to the filters provided
    sort()
    x = 0
    z = 0
    if os.path.isfile('osm.html'):
        os.remove('osm.html')
    print (mapOrders)
    print(filters)
    # Executes this code if a pin map is requested
    if(filters[2] == '2'):
        # Create a folium map object, centred and zoomed on the United Kingdom
        map = folium.Map(location=[55,-2], zoom_start=10)
        # Iterates through each item in the mapOrders array
        while x < len(mapOrders):
            #Requests a latitude and longitude for each order from geocoder.py and
            location = geocoder(mapOrders[x])
            mapOrders[x].append(location)
            print(mapOrders[x][22])
            try:
                if(re.search('[a-zA-Z]', mapOrders[x][22][0])) or (re.search('[a-zA-Z]', mapOrders[x][22][1])):
                    print("marker could not be added, regex")
                else:
                    try:
                        map.circle_marker(location=mapOrders[x][22], radius=20,popup=str(datetime.datetime.date(orderDates[x])) + " address:" + addresses[x] + " lat & long" + str(mapOrders[x][22]) , line_color='black',fill_color='#66ff33', fill_opacity=2.0)
                        print("marker added")
                    except BaseException as e:
                        print("marker could not be added, lat long issue")
            except BaseException as e:
                print ("marker could not be added, index issue")
            x = x + 1
        map.create_map(path='osm.html')
        #print(orderDates)
        ##print(mapOrders)
        print(filters)
        # Executes this code if a heat map is requested
    elif(filters[2]=='1'):
        global heatOrders
        heatOrders = []
        heatMap.clear()
        while x < len(mapOrders):
            location = geocoder(mapOrders[x])
            y = 0
            if(len(heatOrders) == 0):
                heatOrders.append([])
                heatOrders[z].append(location)
                heatOrders[z].append(0.2)
                z = z + 1
            elif(z > len(mapOrders)):
                break
            else:
                y = 0
                while y < len(heatOrders):
                    #print(heatOrders)
                    #print(len(mapOrders))
                    #print(str(y))
                    #print(str(x))
                    #print(str(z))
                    if(location == heatOrders[y][0]):
                        heatOrders[y][1] = heatOrders[y][1] + 0.2
                        print("Duplicate location")
                        x = x + 1
                        y = 0
                        break
                    else:
                        y = y + 1
                if(y == len(heatOrders)):
                    heatOrders.append([])
                    print(heatOrders)
                    heatOrders[z].append(location)
                    heatOrders[z].append(0.2)
                    print(len(heatOrders))
                    print(z)
                    z = z + 1
            x = x + 1
        x = 0
        while(x < len(heatOrders)):
            try:
                if(re.search('[a-zA-Z]', heatOrders[x][0][0])) or (re.search('[a-zA-Z]', heatOrders[x][0][1])):
                    print("Spot could not be added")
                else:
                    try:
                        heatMap.addSpot(heatOrders[x][0][0],heatOrders[x][0][1],heatOrders[x][1])
                    except BaseException as e:
                        print("Spot could not be added")
            except BaseException as e:
                print("Spot could not be added")
            x = x + 1
        if(sslInput == "Y"):
            heatMap.createMap(hostIp,"https://")
        else:
            heatMap.createMap(hostIp,"http://")
    with open('taskStatus.html','w') as f:
        f.write('complete')
    f.close()
@route("/map")
def mapPage():
    return static_file('map.html',root='')
# When the user sets their filters they are submitted to this function
@route("/filters", method='POST')
def setFilters():
    # Clears the filters array so that multiple requests can be made and the correct result is returned for each request
    global filters
    filters = []
    #Adds the start date and end date of the timeframe to be filtered to the filters array
    filters.append(datetime.datetime.strptime(str(request.forms.get('startDate')), "%Y-%m-%d"))
    filters.append(datetime.datetime.strptime(str(request.forms.get('endDate')), "%Y-%m-%d"))
    #Adds the map type and geocoding accuracy requested to the filters array
    filters.append(str(request.forms.get('mapType')))
    filters.append(str(request.forms.get('mapAccuracy')))
    #Adds the product category to the filters array
    filters.append(str(request.forms.get('productCategory')).strip())
    print(filters)
    if(filters[0]>filters[1]):
        return "Invalid date range"
    with open('taskStatus.html','w') as f:
        f.write('incomplete')
    f.close()
#Starts a new thread that executes mappingFunction
    t = threading.Thread(target=mappingFunction)
    t.start()
#Displays a loading page
    return '<meta http-equiv="refresh" content="0; url=/static/loading.html">'


# If the client browser requests the login page this code will be executed:
@route("/login")
def loginPage():
    return static_file('login.html',root='')
# When the user enters a username and passcode the input form passes the data on to the authcheck page and this code is executed:
@route("/authCheck", method='POST')
def authCheck():
# Ensures that data collected from the form is in the format of a string for both fields
    username = str(request.forms.get('username'))
    password = str(request.forms.get('password'))
# Logs the request in the terminal window
    print ("Request from client using username " + username + " and password " + password)
# Initialises the user array
    userArray = []
# Opens the database of users and reads the contents into the user array
    with open('users.csv', 'r') as f:
        while True:
            line = f.readline()
            if not line: break
            user = line.split(',')
            userArray.append(user)
        print (userArray)
    f.close()
    x = 0
# Checks the username and password provided by the user to see if they match any entries in the user array
    while(x < len(userArray)):
# If an entry matches then the request is approved and the user is redirected to the options page
        if(userArray[x][0].strip() == username and userArray[x][1].strip() == password):
            print("Request approved")
#User is checked to see if they are an admin and then redirected to the correct options page
            if(userArray[x][2].strip() == "admin"):
                return '<meta http-equiv="refresh" content="0; url=/static/adminOptions.html">'
            else:
                return '<meta http-equiv="refresh" content="0; url=/static/options.html">'
        else:
            x = x + 1
#If no entries match then the request is denied
    print("Request denied")
    return 'User could not be authenticated. Username or password was incorrect.'
# If the client browser requests the upload page this code will be executed:
@route("/upload")
def uploadPage():
    return static_file('upload.html', root='')
# When the user uploads a file the following code is executed:
@route("/fupload", method='POST')
def fileUpload():
    upload = request.files.get('upload')
# The filename and file format are split into two separate variables
    name, ext = os.path.splitext(upload.filename)
# If the file format is not a CSV then the upload is rejected
    if ext not in ('.csv'):
        return 'File extension not allowed. Data must be in CSV format.'
# If the file is in CSV format then the uploaded file is saved and the user is redirected to the map page
    if os.path.isfile('ordersDatabase.csv'):
        os.remove('ordersDatabase.csv')
        upload.save('ordersDatabase.csv')
    else:
        upload.save('ordersDatabase.csv')
    return '<meta http-equiv="refresh" content="0; url=/map">'
# Processing module:
def sort():
    global orders
    global mapOrders
#Clears the orders array to ensure that no data is left over from the last request
    orders = []
#Clears the map orders array
    mapOrders = []
# Ensures that the global variable orderDates is used, rather than using a local version
    global orderDates
    global filters
    orderDates = []
# Opens the database of orders and reads the orders into the orders array
    with open('ordersDatabase.csv','r') as f:
        while True:
            line = f.readline()
            if not line: break
            order = line.split(',')
            orders.append(order)
        print(orders)
    f.close()
    print(orders)
# Sets x equal to 1 so that the first line of the orders database is ignored as it contains the column headings only
    x = 1
    orderDates.append("")
# While loop iterates through the orders array
    while(x < len(orders)):
# Checks that postcode is valid
        if(re.search('^(GIR ?0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]([0-9ABEHMNPRV-Y])?)|[0-9][A-HJKPS-UW]) ?[0-9][ABD-HJLNP-UW-Z]{2})$',orders[x][15].strip())):
# Converts the order date into a time object
            orderDate = datetime.datetime.strptime(orders[x][2], "%d/%m/%y")
            print(orderDate)
# Compares the order date with the start date and end date of the timeframe to ensure it lies between them
            if(orderDate > filters[0]) and (orderDate < filters[1]):
                
# If the order was made during the timeframe requested and it fits the order category requested then it is added to mapOrders
                if(filters[4] == "All"):
                    mapOrders.append(orders[x])
                    orderDates.append(orderDate)
                    print("Order added")
                elif(orders[x][21].strip() == filters[4]):
                    mapOrders.append(orders[x])
                    orderDates.append(orderDate)
                    print("Order added")
                else:
                    
                    print("Order filtered out")
            else:
                print("Order filtered out")
    
        else:
            print(orders[x][15])
            print("Order filtered out,regex.")
        
        x = x + 1
    #print(mapOrders)
# 0 is returned to signify that the function has executed correctly
    return 0
def geocoder(order):
    geocodeAccuracy = filters[3]
    if(int(geocodeAccuracy) == 2):
        address = urllib.parse.quote_plus(order[15])
    elif(int(geocodeAccuracy) == 1):
        address = urllib.parse.quote_plus(order[14] + ", " + order[15] + ", UK")
    else:
        print(geocodeAccuracy)
        return 'Invalid options'
    try:
        global addresses
        addresses.append(address)
        response = urllib.request.urlopen('http://localhost:80/geoCodeRequest/' + geocodeAccuracy + '/' + address).read().decode('utf-8')
        cleanResponse = response.replace('"','')
        coords = cleanResponse.split(",")
        return coords
    except BaseException as e:
        return "Could not geocode address"
@route("/adminprocess", method='POST')
def adminProcess():
    username = str(request.forms.get('username'))
    action = str(request.forms.get('action'))
    if(action == "1"):
        password = str(request.forms.get('password'))
        result = addUser(username,password)
    elif(action == "2"):
        result = removeUser(username)
    elif(action == "3"):
        result = makeAdmin(username)
    elif(action == "5"):
        categoryName = str(request.forms.get('catName'))
        fieldName = str(request.forms.get('fieldName'))
        result = addCategory(categoryName,fieldName)
    elif(action == "6"):
        newPassword = str(request.forms.get('password'))
        result = changePassword(username,newPassword)
    else:
        result = removeAdmin(username)
    return result
@route("/help_files/<sfile>")
def loadHelpFiles(sfile):
    print("returning help files")
    return static_file(sfile,root='help_files/')
def addUser(username,password):
    userArray = []
    # Opens the database of users and reads the contents into the user array
    with open('users.csv', 'r') as f:
        while True:
            line = f.readline()
            if not line: break
            user = line.split(',')
            userArray.append(user)
    f.close()
    x = 0
    while(x < len(userArray)):
        if (userArray[x][0] == username):
            return "User already exists"
        else:
            x = x + 1
    user = [username,password,"normal"]
    userArray.append(user)
    x = 0
    f = open('users.csv','w')
    while(x<len(userArray)):
        
        f.write(userArray[x][0] + "," + userArray[x][1] + "," + userArray[x][2].strip() + "\n")
        x = x + 1
    f.close()
    return "User added"
def removeUser(username):
    userArray = []
    # Opens the database of users and reads the contents into the user array
    with open('users.csv', 'r') as f:
        while True:
            line = f.readline()
            if not line: break
            user = line.split(',')
            userArray.append(user)
    f.close()
    x = 0
    while(x < len(userArray)):
        if (userArray[x][0] == username):
            userArray[x] = ""
        x = x + 1
    print(userArray)
    while(x<len(userArray)):
        try:
            f.write(userArray[x][0] + "," + userArray[x][1] + "," + userArray[x][2].strip() + "\n")
            x = x + 1
        except BaseException as e:
            x = x + 1
    f.close()
    return "User removed"

def makeAdmin(username):
    userArray = []
    # Opens the database of users and reads the contents into the user array
    with open('users.csv', 'r') as f:
        while True:
            line = f.readline()
            if not line: break
            user = line.split(',')
            userArray.append(user)
    f.close()
    x = 0
    while(x < len(userArray)):
        if (userArray[x][0].strip() == username.strip()):
            userArray[x][2] = "admin"
            x = x + 1
        else:
            x = x + 1
    x = 0
    print(userArray)
    stringToWrite = ""
    with open('users.csv','w') as f:
        while(x<len(userArray)):
            stringToWrite = stringToWrite + userArray[x][0] + "," + userArray[x][1] + "," + userArray[x][2].strip() + "\n"
            print(stringToWrite)
            x = x + 1
        f.write(stringToWrite)
    f.close()
    return "User made into an admin"

def removeAdmin(username):
    userArray = []
    # Opens the database of users and reads the contents into the user array
    with open('users.csv', 'r') as f:
        while True:
            line = f.readline()
            if not line: break
            user = line.split(',')
            userArray.append(user)
    f.close()
    x = 0
    while(x < len(userArray)):
        if (userArray[x][0].strip() == username.strip()):
            userArray[x][2] = "normal"
            x = x + 1
        else:
            x = x + 1
    x = 0
    print(userArray)
    stringToWrite = ""
    with open('users.csv','w') as f:
        while(x<len(userArray)):
            stringToWrite = stringToWrite + userArray[x][0] + "," + userArray[x][1] + "," + userArray[x][2].strip() + "\n"
            print(stringToWrite)
            x = x + 1
        f.write(stringToWrite)
    f.close()
    return "User's admin status removed"

def addCategory(categoryName,fieldName):
    categories = []
    with open('categories.csv', 'r') as f:
        while True:
            line = f.readline()
            if not line: break
            cat = line.split(',')
            categories.append(cat)
    f.close()
    fieldNames = []
    with open('fieldNames.csv', 'r') as f:
        while True:
            line = f.readline()
            if not line: break
            field = line.split(',')
            fieldNames.append(field)
    f.close()
    fieldNameArray = []
    categoryNameArray = []
    fieldNameArray.append(fieldName)
    categoryNameArray.append(categoryName)
    fieldNames.append(fieldNameArray)
    categories.append(categoryNameArray)
    with open('map.html','w') as f:
        x = 0
        mapHtml = '''
            <html>
            <head>
            <style>
            .menuBar{
            background-color: #EEEEEE;
            border-radius: 0px;
            border: 1px solid rgb(0, 0, 0);
            box-shadow: 0 0 2px 0 black;
            height: 90px;
            opacity: 1;
            width: 100%;
            }
            .menuBarText{
            color: #333;
            font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-size: 20px;
            font-style: normal;
            font-weight: 400;
            height: 24px;
            letter-spacing: normal;
            line-height: 1.4em;
            margin-top: -4px;
            opacity: 1;
            text-align: left;
            }
            .filters{
            display:inline-block;
            }
            </style>
            <div class='menuBar'>
            
            <form class='filters' method='POST' action='/filters'>
            <p class='menuBarText filters'>Timeframe (eg. 2016-1-1):</p>
            <input class='filters' type='date' name='startDate'autofocus/>
            <p class='formText filters'> to </p>
            <input class='filters' type='date' name='endDate'/>
            <p class='menuBarText filters'>Map type:</p>
            <select class='filters' name='mapType'>
            <option value='1'>Heat map</option>
            <option value='2'>Pin map</option>
            </select>
            <p class='menuBarText filters'>Map accuracy:</p>
            <select class='filters' name='mapAccuracy'>
            <option value='1'>Town level</option>
            <option value='2'>Postcode regions</option>
            </select>
            <p class='menuBarText filters' >Product category:</p>
            <select class='filters'name='productCategory'>
            '''
        print(fieldNames)
        print(categories)
        while(x < len(categories)):
            print(fieldNames[x][0])
            mapHtml = mapHtml + "<option value='" + fieldNames[x][0] + "'>" + categories[x][0] + "</option>"
            x = x + 1
        mapHtml = mapHtml  + '''
            </select>
            <input type='submit'/>
            </form>
            </div>
            </head>
            <body>
            <iframe style='width:100%;height:100%' src='static/osm.html' frameBorder='0'>
            </iframe>
            </body>
            </html>
            '''
        f.write(mapHtml)
    f.close()
    with open('fieldNames.csv','a') as f:
        f.write("\n" + fieldName)
    f.close()
    with open('categories.csv','a') as f:
        f.write("\n" + categoryName)
    f.close()
    return '<meta http-equiv="refresh" content="0; url=/map">'
def changePassword(username,newPassword):
    userArray = []
    # Opens the database of users and reads the contents into the user array
    with open('users.csv', 'r') as f:
        while True:
            line = f.readline()
            if not line: break
            user = line.split(',')
            userArray.append(user)
    f.close()
    x = 0
    while(x < len(userArray)):
        if (userArray[x][0].strip() == username.strip()):
            userArray[x][1] = newPassword
            x = x + 1
        else:
            x = x + 1
    x = 0
    print(userArray)
    stringToWrite = ""
    with open('users.csv','w') as f:
        while(x<len(userArray)):
            stringToWrite = stringToWrite + userArray[x][0] + "," + userArray[x][1] + "," + userArray[x][2].strip() + "\n"
            print(stringToWrite)
            x = x + 1
        f.write(stringToWrite)
    f.close()
    return "User's password changed"
# Start listening for requests
if(sslInput == "Y"):
    srv = SSLWSGIRefServer(host=hostIp, port=port)
    run(server=srv)
else:
    run(host=hostIp, port=port)